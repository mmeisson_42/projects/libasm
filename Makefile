
NAME		= libfts.a


CC			= nasm
UNAME		= $(shell uname)


ifeq ($(UNAME), Linux)
	NFLAGS	= -f elf64 -I./incs/ -d linux
else
	NFLAGS	= -f macho64 --prefix _ -I./incs/ -d macos
endif

NFLAGS		+= -Wall

SRCS_NAMES	= ft_islower.s ft_isupper.s ft_bzero.s ft_isalpha.s ft_isdigit.s \
			  ft_isalnum.s ft_isascii.s ft_isprint.s ft_tolower.s ft_toupper.s \
			  ft_strlen.s ft_strcpy.s ft_strcat.s ft_puts.s ft_memcpy.s \
			  ft_memset.s ft_strdup.s \
			  ft_cat.s ft_fcat.s

# ft_puts.s Doesn't work for unknown reason

OBJS_PATH	= ./.objs/
SRCS_PATH	= ./srcs/

OBJS_NAMES	= $(SRCS_NAMES:.s=.o)

SRCS		= $(addprefix $(SRCS_PATH),$(SRCS_NAMES))
OBJS		= $(addprefix $(OBJS_PATH),$(OBJS_NAMES))

DEPS		= $(OBJS:.o=.d)



all: $(NAME) test


$(NAME): $(OBJS)
	ar rc $(NAME) $(OBJS)

$(OBJS_PATH)%.o: $(SRCS_PATH)%.s Makefile
	@mkdir -p $(OBJS_PATH)
	$(CC) $(NFLAGS) $< -o $@ -MD $(OBJS_PATH)$*.d

clean:
	make -C tests fclean
	rm -f libfts_test
	rm -rf $(OBJS_PATH)

fclean:
	make -C tests fclean
	rm -f libfts_test
	rm -rf $(OBJS_PATH)
	rm -f $(NAME)

re: fclean all

test:
	rm -f libfts_test
	make -C tests
	ln -s tests/libfts_test libfts_test

-include $(DEPS)
