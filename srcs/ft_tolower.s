
section .text

global ft_tolower
extern ft_isupper

; int	ft_tolower(int c);

ft_tolower:
	call ft_isupper
	cmp rax, 0
	mov rax, rdi
	je ret
	add rax, 32
	ret

ret:
	ret
