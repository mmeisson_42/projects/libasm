
section .text
	global ft_strcpy

ft_strcpy:
	mov r11, rdi				; save rdi in r11 to return later
	cmp byte [rsi], 0
	je _ret

cpy:
	cld
	movsb
	cmp byte [rsi], 0
	jne cpy

_ret:
	mov byte [rdi], 0			; set trailing '\0'
	mov rax, r11				; return saved start's destination
	ret
