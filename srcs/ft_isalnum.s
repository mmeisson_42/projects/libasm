
section .text

global ft_isalnum
extern ft_isalpha
extern ft_isdigit

; int	ft_isalnum(int c);

ft_isalnum:
	call ft_isdigit
	cmp rax, 0
	jne ret
	call ft_isalpha

ret:
	ret
