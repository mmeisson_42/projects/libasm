
section .text

global ft_strcat
extern ft_strlen
extern ft_strcpy

; char	*ft_strcat(char *dst, const char *src);

ft_strcat:
	push rbp
	mov rbp, rsp
	sub rsp, 10h

	mov [rbp - 8], rdi
	mov [rbp - 16], rsi
	call ft_strlen
	mov rdi, [rbp - 8]
	mov rsi, [rbp - 16]

	add rdi, rax
	call ft_strcpy
	mov rax, [rbp - 8]

	add rsp, 10h
	pop rbp
	ret
