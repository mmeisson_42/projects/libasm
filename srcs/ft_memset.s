
section .text

	global ft_memset

;void	*ft_memset(void *dst, int c, unsigned long n);

ft_memset:
	mov r11, rdi		; save rdi to ret it later
	cmp rdx, 0
	je ret

	mov eax, esi		; rax is the register stos copy from
	and eax, 0xff
	cld
	mov rcx, rdx

	rep stosb

ret:
	mov rax, r11		; ret initial rdi
	ret
