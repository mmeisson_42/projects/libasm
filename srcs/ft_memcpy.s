
section .text
	global ft_memcpy

;void	*ft_memcpy(void *dst, const void *src, unsigned int l);

ft_memcpy:
	mov r8, rdi		; save the ret value

	cmp rdx, 0
	je ret					; Do not work if you don't have to (l == 0)

	cld						; CLear Direction flag
	mov rcx, rdx			; rdx contains the idiv result || the originally small len
	rep movsb

ret:
	mov rax, r8
	ret
