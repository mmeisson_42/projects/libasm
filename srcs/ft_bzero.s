
section .text

global ft_bzero

; void	ft_bzero(void *data, size_t len)

ft_bzero:
	cld
	xor rax, rax
	mov rcx, rsi
	rep stosb
	ret