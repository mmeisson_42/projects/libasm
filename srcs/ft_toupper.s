
section .text

global ft_toupper
extern ft_islower

; int	ft_toupper(int c);

ft_toupper:
	call ft_islower
	cmp rax, 0
	mov rax, rdi
	je ret
	sub rax, 32
	ret

ret:
	ret
