
section .text

global ft_isascii

; int	ft_isascii(int c);

ft_isascii:
	cmp rdi, 0
	jl error
	cmp rdi, 127
	jg error
	mov rax, 1
	ret

error:
	mov rax, 0
	ret
