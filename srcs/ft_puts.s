
%include "compatibility.asm"

section .rodata
	newline db 10, 0
	null db "(null)", 0

section .text
	global ft_puts
	extern ft_strlen

ft_puts:
	push rbp
	mov rbp, rsp
	sub rsp, 10h

%ifndef linux
	cmp rdi, 0						; macos handle null ptrs, linux segv
	je handle_null

call_puts:
%endif
	mov r11, rdi
	call ft_strlen

	mov rdi, 1						; store fd(1) in write(fd,,)
	mov rsi, r11					; store str im write(,str,)
	mov rdx, rax					; store strlen(str) in write(,,len)
	mov rax, SYSCODE(WRITE)
	syscall
	mov [rbp - 8], rax

	mov rdi, 1						; store fd(1) in write(fd,,)
	lea rsi, [rel newline]			; store "\n" in write(,"\n",)
	mov rdx, 1						; store fd(1) in write(fd,,)
	mov rax, SYSCODE(WRITE)
	syscall
	add rax, [rbp - 8]

	add rsp, 10h
	pop rbp
	ret

%ifndef linux
handle_null:
	lea rdi, [rel null]	; replace the NULL with string's address
	jmp call_puts
%endif
