
%include "compatibility.asm"

%define BUFF_SIZE 4096

section .text
	global ft_cat

;	buffer	: rbp - 4112 ( to 16 == BUFF_SIZE bytes )
;	fd		: rbp - 8

ft_cat:
	push rbp
	mov rbp, rsp
	sub rsp, BUFF_SIZE + 16

	mov [rbp - 8], rdi

loop:
	mov rdi, [rbp - 8]
	mov rsi, rbp
	sub rsi, BUFF_SIZE + 16
	mov rdx, BUFF_SIZE
	mov rax, SYSCODE(READ)
	syscall
	jc ret
	cmp rax, 0
	je ret
	mov rdi, 1
	mov rsi, rbp
	sub rsi, BUFF_SIZE + 16
	mov rdx, rax
	mov rax, SYSCODE(WRITE)
	syscall
	jc ret
	jmp loop

ret:
	add rsp, BUFF_SIZE + 16
	pop rbp
	ret
