
section .text

global ft_isupper

; int	ft_isupper(int c);

ft_isupper:
	cmp rdi, 'A'
	jl error
	cmp rdi, 'Z'
	jg error
	mov rax, 1
	ret

error:
	mov rax, 0
	ret
