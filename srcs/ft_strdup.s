
section .text
	global ft_strdup
	extern ft_memcpy
	extern ft_strlen
	extern malloc
	
; char	*ft_strdup(const char *str);

ft_strdup:
	push rbp
	mov rbp, rsp
	sub rsp, 10h

	mov [rbp - 8], rdi
	call ft_strlen
	inc rax

;call_malloc
	mov [rbp - 16], rax		; save strlen(str)+1 to use memcpy later instead of strcpy
	mov rdi, rax

	call malloc

;call_memcpy
	mov rdi, rax
	mov rsi, [rbp - 8]		; retrieve str (as memcpy's src)
	mov rdx, [rbp - 16]		; retrieve strlen(str) + 1
	cmp rax, 0				; if malloc failed, ret
	je ret
	call ft_memcpy

ret:
	add rsp, 10h
	pop rbp
	ret
