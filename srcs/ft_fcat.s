
%include "compatibility.asm"

; FROM <sys/mman.h>
%idefine PROT_NONE 0x00
%idefine PROT_READ 0x01
%idefine PROT_WRITE 0x02
%idefine PROT_EXEC 0x03

%idefine MAP_FILE 0x0000
%idefine MAP_PRIVATE 0x0002

section .data

deb: db "coucou size = %ld", 10, 0

section .text
	global ft_fcat
	extern malloc

; void	fcat(int fd);

; [rbp - 8] == file_descriptor
; [rbp - 16] == mmaped file's string ptr
; [rbp - 160] == struct stat (sizeof(struct stat) == 144 + 16 == 160)

; st_size's offset ==  72
; [rbp - 88] = st_size       ( 160 - 72 == 88 )

ft_fcat:
	push rbp
	mov rbp, rsp
	sub rsp, 0xA0

	mov [rbp - 8], rdi				; save file_descriptr

; call fstat
	; mov rdi, rdi					; fd
	mov rsi, rbp					; &(struct stat)
	sub rsi, 160
	mov rax, SYSCODE(FSTAT)
	syscall
	jc ret

; call mmap
	mov rdi, 0						; NULL,
	mov rsi, [rbp - 88]				; st_size (file's len)
	mov rdx, PROT_READ				; PROT_READ
	mov rcx, MAP_FILE | MAP_PRIVATE	; MAP_FILE | MAP_PRIVATE
	mov r8, [rbp - 8]				; file_descriptor
	mov r9, 0						; 0
	mov rax, SYSCODE(MMAP)
	syscall
	cmp rax, 0
	jle ret
	mov [rbp - 16], rax			; save file's content


; call write
	mov rdi, 1						; stdin
	mov rsi, rax					; str_file
	mov rdx, [rbp - 88]				; file's len
	mov rax, SYSCODE(WRITE)
	syscall

; call munmap
	mov rdi, [rbp - 16]				; file's content
	mov rsi, [rbp - 88]				; file's len
	mov rax, SYSCODE(MUNMAP)
	syscall

ret:
	add rsp, 0xA0
	pop rbp
	ret
