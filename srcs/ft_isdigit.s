
section .text

global ft_isdigit

; int	ft_isdigit(int c);

ft_isdigit:
	cmp rdi, '0'
	jl error
	cmp rdi, '9'
	jg error
	mov rax, 1
	ret

error:
	mov rax, 0
	ret
