
section .text

global ft_islower

; int	ft_islower(int c);

ft_islower:
	cmp rdi, 'a'
	jl error
	cmp rdi, 'z'
	jg error
	mov rax, 1
	ret

error:
	mov rax, 0
	ret
