
section .text

	global ft_strlen

ft_strlen:
	xor rax, rax
	mov rcx, -1
	cld					; CLear Direction flag
	repne scasb
	not rcx				; reverse rcx ( 0xfb (1111 1011) [-4] -> 0x04 (0000 0100) [4])
	dec rcx
	mov rax, rcx
	ret
