
section .text

global ft_isalpha
extern ft_islower
extern ft_isupper

; int	ft_isalpha(int c);

ft_isalpha:
	call ft_islower
	cmp rax, 0
	jne ret
	call ft_isupper

ret:
	ret
