
%idefine STDIN		0
%idefine STDOUT		1
%idefine STDERR		2

; Following are syscall number that have same value in macos and linux
%idefine READ			0x03
%idefine WRITE			0x04
%idefine OPEN			0x05
%idefine CLOSE			0x06


%ifdef linux
%define SYSCODE(val)	val

%idefine FSTAT			0x05
%idefine MMAP			0xC5
%idefine MUNMAP			0x49

%elifdef macos
%define SYSCODE(val)	0x2000000 | val

%idefine FSTAT			0xBD
%idefine MMAP			0xC5
%idefine MUNMAP			0x49

%endif

