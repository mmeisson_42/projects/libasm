
#include "tests_lib.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char		*ft_strcpy(char *dst, const char *src);

static int		unit_test_strcpy(char *test)
{
	char const	fmt[] = "\033[0%dm%s\033[0m test ft_strcpy on string '%s'";
	char const	*status_str;
	int			status, status_color;
	char		*str_ft, *str_off;
	char		*res;
	int			diff;
	int			fd;

	str_ft = malloc(strlen(test) + 1);
	str_off = malloc(strlen(test) + 1);
	res = ft_strcpy(str_ft, test);
	strcpy(str_off, test);
	diff = strcmp(str_off, str_ft);
	if (res != str_ft) {
		fd = 2;
		status_str = FAILURE" res does not match";
		status_color = RED_OCTAL;
		status = ERROR;
	}
	else if (diff == 0) {
		fd = 1;
		status_str = SUCCESS;
		status_color = GREEN_OCTAL;
		status = WORKS;
	}
	else {
		fd = 2;
		status_str = FAILURE;
		status_color = RED_OCTAL;
		status = ERROR;
	}

#if VERBOSE == NORMAL || VERBOSE == FULL
	dprintf(fd, fmt, status_color, status_str, test);
# if VERBOSE == FULL
	dprintf(fd, "(off: '%s', mine: '%s')", str_off, str_ft);
# endif
	dprintf(fd, "\n");
#endif
	if (str_off) {
		free(str_off);
	}
	if (str_ft) {
		free(str_ft);
	}
	return status;
}

int			test_strcpy(void)
{
	char	strs[5][1024] = {
		"",
		"a",
		"ab",
		"abc",
		"abaabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcabcbcc",
	};
	int status = WORKS;

	for (unsigned long i = 0; i < TAB_LEN(strs); i++) {
		if (unit_test_strcpy(strs[i]) == ERROR) {
			status = ERROR;
		}
	}
	return status;
}
